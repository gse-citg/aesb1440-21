AESB1440-21
===========

Notebook to visualize the DualEM data in **Jupyter Lab/Notebook**.

If you have Jupyter installed, you can download the notebook
``plot_Dualem_data.ipynb`` and run it in your local installation. You need to
have ``scipy`` and ``matplotlib`` installed.


Notebooks
---------

- `plot_Dualem_data.ipynb`: Main notebook to plot your measured data and draw
  some conclusions.
- `invert_with_lookuptable.ipynb`: Notebook to invert the data with a
  lookuptable to obtain the best fitting 2-layer model.


Online possibilities
--------------------

**Note**: For Google Colab / JupyterLite, you have to download the notebook to your
computer, and upload it and your data csv-file to the service (Google Colab or
JupyterLite). For Binder, you only have to upload your data csv-file, as the
notebook should be there already. Also note, these are all temporary providers,
not permanent. Make sure you save your important data (e.g., figures, notebook)
to your computer before closing them.



- .. image:: https://mybinder.org/badge_logo.svg
      :target: https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.tudelft.nl%2Fgse-citg%2Faesb1440-21/HEAD?labpath=plot_Dualem_data.ipynb
      :alt: MyBinder

  Binder: You can run the notebook on MyBinder; it might take some time to
  start up.

- .. image:: https://colab.research.google.com/assets/colab-badge.svg
     :target: https://colab.research.google.com/
     :alt: Colab

  Google Colab: If you have a Google account, you can login to Google Colab,
  and upload the notebook there.


- .. image:: https://jupyterlite.rtfd.io/en/latest/_static/badge-launch.svg
    :target: https://emsig.xyz/emlite
    :alt: JupyterLite

  JupyterLite: You can upload the notebook and your data to this static
  website, and run the notebook. This runs in your browsers cache. If you
  delete your cache, restart your computer, or similar, everything will be
  lost. Make sure to download your results!
